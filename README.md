This repositiory is for internal use of SmartyStreet(SS) API.

Exported Functions:

1. autoComplete:
	Parameters: config, searchText, selectedAddress
		config: object of the form
		{
		clientKey: <Contains your SS client key>
		referer: <Referer url for client key>
		}
		searchText: Text for which you want address suggestions
		selectedAddress (optional): Address object to expand secondary addresses.

		Response: array of address suggestions.
