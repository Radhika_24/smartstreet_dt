import { autoComplete } from "../functions/index";

test("Auto suggest", async () => {
	try {
		jest.setTimeout(30000);

		const config = {
			clientKey: "113578612793854719",
			referer: "ubiquitousinfluence.com",
		};
		const searchText = "123 mai";
		const res = await autoComplete(config, searchText);
		console.log(`res`, res);
		expect(res.suggestions).toBeDefined();
	} catch (e) {
		console.log(e);
	}
});
