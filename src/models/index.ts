export interface Config {
	clientKey: string;
	referer: string;
}

export interface APIRequest {
	headers: any;
	endpoint: string;
	data?: any;
	method?: "GET" | "POST" | "PUT" | "DELETE";
}
