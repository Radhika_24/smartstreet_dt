const https = require("https");

import { APIRequest } from "../models";

export const sendAPIRequest = async ({
	headers,
	endpoint,
	method = "GET",
}: APIRequest) => {
	const options = {
		port: 443,
		method,
		headers: headers,
	};

	return new Promise((resolve, reject) => {
		const req = https.request(endpoint, options, function (res: any) {
			res.on("data", function (data: any) {
				resolve(JSON.parse(data));
			});
		});

		req.on("error", function (e: any) {
			reject(e);
		});
		req.end();
	});
};
