import { APIRequest } from "../models";
export declare const sendAPIRequest: ({ headers, endpoint, method, }: APIRequest) => Promise<unknown>;
