import { Config } from "../models";
import { AUTOCOMPLETE_URL } from "../shared/constants";
import { sendAPIRequest } from "../shared/sendApiRequest";

export const autoComplete = async (
	config: Config,
	searchText: string,
	selectedAddress?: any
): Promise<any> => {
	const { clientKey, referer } = config;
	let url;
	if (selectedAddress) {
		const selected = `${selectedAddress.street_line} ${selectedAddress.secondary} (${selectedAddress.entries}) ${selectedAddress.city} ${selectedAddress.state} ${selectedAddress.zipcode}`;
		url = `${AUTOCOMPLETE_URL}?key=${clientKey}&search=${searchText}&selected=${selected}`;
	} else {
		url = `${AUTOCOMPLETE_URL}?key=${clientKey}&search=${searchText}`;
	}
	const headers = {
		referer,
	};

	const res = await sendAPIRequest({ headers, endpoint: url });
	return res;
};
